package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	
	private static final int MAX_ROLLS = 20;
	private int[] rolls= new int[21];
	private int currentRoll=0;
	
	@Override
	public void roll(int pins) {
		rolls[currentRoll++]=pins;
	}

	@Override
	public int score() {
		int score=0;
		if(partitaPerfetta())
			for(int currentRoll=0;currentRoll<10;currentRoll++){
				if(rolls[currentRoll]==10)
					score+=rolls[currentRoll+1]+rolls[currentRoll+2];
				
				/*if(currentRoll==10 && rolls[currentRoll]==10)
					score+=rolls[currentRoll]+rolls[currentRoll+1];
				
				else */
					score+=rolls[currentRoll];
			}

			
			
		else	
			for(int currentRoll=0;currentRoll<MAX_ROLLS+1;currentRoll++){
				if(isSpare(currentRoll))
					score+=rolls[currentRoll+2];
				if(isStrike(currentRoll))
					score+=rolls[currentRoll+1]+rolls[currentRoll+2];
				if(currentRoll==18 && rolls[currentRoll]==10)
					score+=rolls[currentRoll+1]+rolls[currentRoll+2];
				else 
					score+=rolls[currentRoll];}
		return score;
	}

	private boolean isSpare(int currentRoll){
		return (currentRoll<MAX_ROLLS-1 && currentRoll%2==0 &&(rolls[currentRoll]+rolls[currentRoll+1]==10) );
	}
	
	private boolean isStrike(int currentRoll){
		return (currentRoll<MAX_ROLLS-1 && currentRoll%2==0 &&(rolls[currentRoll]==10) );
	}
	
	private boolean partitaPerfetta(){
		boolean partita=true;
		for(int i=0;i<12;i++)
				if(rolls[i]!=10)
					partita=false;
		return partita;
	}
}
